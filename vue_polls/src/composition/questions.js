import {ref, onMounted} from 'vue';
import {api, NotFoundError} from '../helpers/API'

export function useFetchList () {
  const questions = ref([])
  const loading = ref(false)
  const error = ref(false)
  const fetchData = async function () {
    loading.value = true
    try {
      questions.value = await api('/questions/', 'get')
    } catch (e) {
      if (e instanceof NotFoundError) {
        error.value = 'Page not found'
      }
    }
    loading.value = false
  }
  const delete_question = async function(question_id) {
    try {
      await api('/questions/'+question_id, 'delete')
    } catch (e) {
      if (e instanceof NotFoundError) {
        error.value = 'Page not found'
      }
    }
    questions.value.splice(questions.value.findIndex(question => question.id == question_id), 1)
  }
  onMounted(() => {
    fetchData()
  })
  return {
    questions,
    loading,
    error,
    delete_question
  }
}