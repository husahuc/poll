import {ref} from 'vue';
import {api_post, NotFoundError} from '../helpers/API'

export function useCreateQuestion(router) {
  const choices = ref([{choice_text: "test"}])
  const success = ref(false)
  const question = ref({question_text: "", pub_date: null})
  const error = ref(false)

  const create_question = async function() {
    let return_val = null
    try {
      return_val = await api_post('/questions/', 'POST',
      {"question_text": question.value.question_text,
      "pub_date": new Date()})
    } catch (e) {
      if (e instanceof NotFoundError) {
        error.value = 'Page not found'
      }
    }
    for (var choice of choices.value)
    {
      try {
        await api_post('/questions/' + parseInt(return_val.id) + '/choice', 'POST', {
          choice_text: choice.choice_text,
          votes: 0
        })
      } catch (e) {
        if (e instanceof NotFoundError) {
          error.value = 'Page not found'
        }
      }
    }
    router.push({name: 'Question', params: {id: return_val.id}})
  }
  const add_choice = function() {
    choices.value.push({choice_text:""})
  }
  const remove_choice = function(choice) {
    if (choices.value.length > 1) {
      choices.value.splice(choices.value.indexOf(choice), 1)
    }
  }
  return {
    choices,
    success,
    question,
    create_question,
    add_choice,
    remove_choice
  }
}