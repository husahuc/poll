import { ref, onMounted, computed } from "vue";
import { api, NotFoundError, api_post } from "../helpers/API";

export function useQuestion(props) {
  const data = ref({});
  const loading = ref(true);
  const error = ref(false);
  const edit = ref(false);
  const editData = ref({});

  const total_votes = computed(() => {
    let total = 0;
    if (loading.value == true) {
      return 0;
    }
    data.value.choices.forEach((element) => {
      total += element.votes.length;
    });
    return total;
  });

  const fetchData = async function() {
    loading.value = true;
    try {
      data.value = await api("/questions/" + props.id, "GET");
    } catch (e) {
      if (e instanceof NotFoundError) {
        error.value = "Page not found";
      }
    }
    editData.value = JSON.parse(JSON.stringify(data.value));
    loading.value = false;
  };

  const vote = async function(choice_id) {
    let return_val = null;
    try {
      return_val = await api_post("/choices/" + choice_id + "/vote", "POST", {
        pub_date: new Date(),
      });
    } catch (e) {
      if (e instanceof NotFoundError) {
        error.value = "Page not found";
      }
    }
    data.value.choices
      .find((element) => element.id == choice_id)
      .votes.push(return_val);
    data.value.choices.find((element) => element.id == choice_id).voted = true;
  };
  const unvote = async function(choice_id) {
    let return_val = null;
    try {
      return_val = await api("/choices/" + choice_id + "/unvote", "DELETE");
    } catch (e) {
      if (e instanceof NotFoundError) {
        error.value = "Page not found";
      }
    }
    data.value.choices
      .find((element) => element.id == choice_id)
      .votes.splice(
        data.value.choices
          .find((element) => element.id == choice_id)
          .votes.indexOf(
            (element) => element.id == return_val.detail.vote_value
          ),
        1
      );
    data.value.choices.find((element) => element.id == choice_id).voted = false;
  };

  const update = async function() {
    try {
      await api_post("/questions/" + parseInt(props.id), "PUT", {
        question_text: editData.value.question_text,
        pub_date: new Date(),
      });
    } catch (e) {
      if (e instanceof NotFoundError) {
        error.value = "Page not found";
      }
    }
    for (var choice of editData.value.choices) {
      if (!choice.id) {
        try {
          await api_post(
            "/questions/" + parseInt(props.id) + "/choice",
            "POST",
            {
              choice_text: choice.choice_text,
            }
          );
        } catch (e) {
          if (e instanceof NotFoundError) {
            error.value = "Page not found";
          }
        }
      } else {
        try {
          await api_post("/choices/" + parseInt(choice.id), "PUT", {
            choice_text: choice.choice_text,
            votes: choice.votes,
          });
        } catch (e) {
          if (e instanceof NotFoundError) {
            error.value = "Page not found";
          }
        }
      }
    }
    data.value = editData.value;
    edit.value = false;
  };

  const add_choice = function() {
    editData.value.choices.push({ choice_text: "", votes: 0 });
  };
  const remove_choice = function(choice) {
    if (editData.value.choices.length > 1) {
      editData.value.choices.splice(editData.value.choices.indexOf(choice), 1);
    }
  };

  onMounted(() => {
    fetchData();
  });
  return {
    data,
    loading,
    error,
    vote,
    unvote,
    edit,
    editData,
    add_choice,
    remove_choice,
    update,
    total_votes,
  };
}
