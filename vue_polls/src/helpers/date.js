import moment from 'moment'

export function FormatDate(date) {
  return moment(String(date)).format('MM/DD/YYYY hh:mm')
}