export class NotFoundError extends Error {
}

const SERVER_URL = 'http://127.0.0.1:8000'

export async function api(url, method, body = null) {
  const longurl = SERVER_URL + url
  const r = await fetch(longurl, {
    method: method,
    headers: {
      'Access-Control-Allow-Origin': '*',
      Accept: '*/*',
      'Content-Type': 'application/json'
    },
    body: body
  })
  if (r.status >= 200 && r.status < 400) {
    return await r.json()
  }
  if (r.status == 404) {
    return new NotFoundError()
  }
}

export async function api_post(url, method, body) {
  const longurl = SERVER_URL + url
  const r = await fetch(longurl, {
    method: method,
    headers: {
      'Access-Control-Allow-Origin': '*',
      Accept: '*/*',
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(body)
  })
  if (r.status >= 200 && r.status < 400) {
    return await r.json()
  }
  if (r.status == 404) {
    return new NotFoundError()
  }
}