import { createRouter, createWebHistory } from "vue-router";
import Home from "../views/home.vue";
import QuestionV from "../views/question.vue";

const routes = [
  {
    path: "/",
    name: "Home",
    component: Home
  },
  {
    path: "/question/:id",
    name: "Question",
    component: QuestionV,
    props: true
  }
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
});

export default router;