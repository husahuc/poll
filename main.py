from fastapi import FastAPI, HTTPException, Response, Depends, Request
import schema
from typing import List

from fastapi.middleware.cors import CORSMiddleware

from sqlalchemy.orm import Session

import crud
from database import SessionLocal, engine
from models import Base

Base.metadata.create_all(bind=engine)

app = FastAPI()

origins = [
    "http://localhost",
    "http://localhost:8080",
]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

# Dependency
def get_db():
    try:
        db = SessionLocal()
        yield db
    finally:
        db.close()


## Question

@app.post("/questions/", response_model=schema.QuestionInfo)
def create_question(question: schema.QuestionCreate, db: Session = Depends(get_db)):
	return crud.create_question(db=db, question=question)


@app.get("/questions/", response_model=List[schema.Question])
def get_questions(db: Session = Depends(get_db)):
	return crud.get_all_questions(db=db)

def get_question_obj(db, qid):
	obj = crud.get_question(db=db, qid=qid)
	if obj is None:
		raise HTTPException(status_code=404, detail="Question not found")
	return obj

@app.get("/questions/{qid}", response_model=schema.QuestionInfoRequest)
def get_question_request(qid: int, request: Request, db: Session = Depends(get_db)):
	get_question_obj(db=db, qid=qid)
	return crud.get_question_request(db=db, qid=qid, Request=request)
	
@app.put("/questions/{qid}", response_model=schema.QuestionInfo)
def edit_question(qid: int, question: schema.QuestionCreate, db: Session = Depends(get_db)):
	get_question_obj(db=db, qid=qid)
	obj = crud.edit_question(db=db, qid=qid, question=question)
	return obj

@app.delete("/questions/{qid}")
def delete_question(qid: int, db: Session = Depends(get_db)):
	get_question_obj(db=db, qid=qid)
	crud.delete_question(db=db, qid=qid)
	return {"detail": "Question deleted", "status_code": 204}

# choice

def get_choice_obj(db, choice_id):
	obj = crud.get_choice(db=db, choice_id=choice_id)
	if obj is None:
		raise HTTPException(status_code=404, detail="Choice not found")
	return obj

@app.post("/questions/{qid}/choice", response_model=schema.ChoiceList)
def create_choice(qid: int, choice: schema.ChoiceCreate, db: Session = Depends(get_db)):
	get_question_obj(db=db, qid=qid)
	return crud.create_choice(db=db, qid=qid, choice=choice)

@app.put("/choices/{choice_id}", response_model=schema.ChoiceList)
def edit_choice(choice_id: int, choice: schema.ChoiceCreate, db: Session = Depends(get_db)):
	get_choice_obj(db=db, choice_id=choice_id)
	obj = crud.edit_choice(db=db, choice_id=choice_id, choice=choice)
	return obj

@app.delete("/choices/{choice_id}")
def delete_choice(choice_id: int, db: Session = Depends(get_db)):
	get_choice_obj(db=db, choice_id=choice_id)
	crud.delete_choice(db=db, choice_id=choice_id)
	return {"detail": "Choice deleted", "status_code": 204}

# vote

def check_duplicate_ip(db, choice_id, ip_adress):
	obj = crud.get_all_votes(db=db, choice_id=choice_id)
	for vote in obj:
		if(vote.ip_adress == ip_adress):
			raise HTTPException(status_code=403, detail="Already Voted")

# response_model=schema.VoteInfo
@app.post("/choices/{choice_id}/vote")
def create_vote(choice_id: int, vote: schema.VoteCreate, request: Request, db: Session = Depends(get_db)):
	get_choice_obj(db=db, choice_id=choice_id)
	check_duplicate_ip(db=db, choice_id=choice_id, ip_adress=request.client.host)
	return crud.create_vote(db=db, cid=choice_id, vote=vote, ip_adress=request.client.host)

@app.delete("/choices/{choice_id}/unvote")
def unvote(choice_id: int,  request: Request, db: Session = Depends(get_db)):
	get_choice_obj(db=db, choice_id=choice_id)
	vote_value = crud.unvote(db=db, choice_id=choice_id, ip_adress=request.client.host)
	return {"detail": {"text":"Choice unvoted", 'vote_value':vote_value}, "status_code": 204}

@app.delete("/vote/{vote_id}")
def delete_vote(vote_id: int, db: Session = Depends(get_db)):
	crud.delete_vote(db=db, vote_id=vote_id)
	return {"detail": "Choice deleted", "status_code": 204}