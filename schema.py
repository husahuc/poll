from datetime import datetime

from pydantic import BaseModel
from typing import List

# Vote schema

class VoteBase(BaseModel):
	pub_date: datetime

class VoteCreate(VoteBase):
	pass

class VoteServerCreate(VoteBase):
	ip_adress: str

class Vote(VoteBase):
	id: int

	class Config:
		orm_mode = True

class VoteInfo(Vote):
	ip_adress: str

# Choice schema

class ChoiceBase(BaseModel):
	choice_text: str

class ChoiceCreate(ChoiceBase):
	pass

class ChoiceList(ChoiceBase):
	id: int

	class Config:
		orm_mode = True

class ChoiceVotesList(ChoiceList):
	votes: List[VoteInfo] = []

class ChoiceRequest(ChoiceVotesList):
	voted: bool

# Question schema

class QuestionBase(BaseModel):
	question_text: str
	pub_date: datetime

class QuestionCreate(QuestionBase):
	pass

class Question(QuestionBase):
	id: int

	class Config:
		orm_mode = True

class QuestionInfo(Question):
	choices: List[ChoiceVotesList] = []

class QuestionInfoRequest(Question):
	choices: List[ChoiceRequest] = []