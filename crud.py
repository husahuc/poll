from sqlalchemy.orm import Session

from models import Base, Question, Choice, Vote
import schema

# Question

def create_question(db: Session, question: schema.QuestionCreate):
	obj = Question(**question.dict())
	db.add(obj)
	db.commit()
	return obj

def get_all_questions(db: Session):
	return db.query(Question).all()

def get_question(db:Session, qid):
	return db.query(Question).filter(Question.id == qid).first()

def get_question_request(db:Session, qid, Request):
	obj = db.query(Question).filter(Question.id == qid).first()
	for choice in obj.choices:
		choice.voted=False
		for vote in choice.votes:
			if (vote.ip_adress == Request.client.host):
				choice.voted=True
	return obj

def edit_question(db: Session, qid, question: schema.QuestionCreate):
	obj = db.query(Question).filter(Question.id == qid).first()
	obj.question_text = question.question_text
	obj.pub_date = question.pub_date
	db.commit()
	return obj

def delete_question(db: Session, qid):
	db.query(Question).filter(Question.id == qid).delete()
	db.commit()

# Choice

def create_choice(db:Session, qid: int, choice: schema.ChoiceCreate):
	obj = Choice(**choice.dict(), question_id=qid)
	db.add(obj)
	db.commit()
	return obj

def get_choice(db:Session, choice_id):
	return db.query(Choice).filter(Choice.id == choice_id).first()

def edit_choice(db:Session, choice_id, choice: schema.ChoiceCreate):
	obj =  db.query(Choice).filter(Choice.id == choice_id).first()
	obj.choice_text = choice.choice_text
	db.commit()
	return obj

def delete_choice(db: Session, choice_id):
	db.query(Choice).filter(Choice.id == choice_id).delete()
	db.commit()

# Vote

def get_all_votes(db: Session, choice_id):
	return db.query(Vote).filter(Vote.choice_id == choice_id)

def create_vote(cid: int, db:Session, vote: schema.VoteCreate, ip_adress):
	obj_vote = Vote(**vote.dict(), choice_id=cid, ip_adress=ip_adress)
	db.add(obj_vote),
	db.commit()
	return obj_vote

def delete_vote(db: Session, vote_id):
	db.query(Vote).filter(Vote.id == vote_id).delete()
	db.commit()

def unvote(db: Session, choice_id, ip_adress):
	obj_choice = db.query(Choice).filter(Choice.id == choice_id).first()
	vote_value = None
	for vote in obj_choice.votes:
		if (vote.ip_adress == ip_adress):
			vote_value = vote.id
			db.query(Vote).filter(Vote.id == vote.id).delete()
	db.commit()
	return ({'vote': vote_value})